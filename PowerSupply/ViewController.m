//
//  ViewController.m
//  PowerSupply
//
//  Created by Christopher Cope on 19/06/2020.
//  Copyright © 2020 Christopher Cope. All rights reserved.
//

#import "ViewController.h"
#import <modbus.h>

static double const minimumVoltage = 0.0; //!< Min allowable voltage in V
static double const maximumVoltage = 32.0;//!< Max allowable voltage in V
static double const minimumCurrent = 0.0; //!< Min allowable current in A
static double const maximumCurrent = 5.1; //!< Max allowable current in A

static uint32_t const minimumAutoPollTime = 25;   //!< Min poll time for auto poll in ms
static uint32_t const maximumAutoPollTime = 30000; //!< Max poll time for auto poll in ms
static uint32_t const defaultAutoPollTime = 100;  //!< Default poll time for the auto poll in ms

static uint32_t const regDumpSize = 100;      //!< number of registers to read when doing a reg dump
static uint32_t const regDumpChunkSize = 100; //!< Must be <= @ref regDumpSize and <= than 100

static uint32_t const logWindowLineLimit = 100; //!< Max number of lines in the log window

@implementation ViewController

/**
  What it says, i.e init the thing
 */
- (instancetype)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

/**
 Tidy up
 */
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/**
  Startup stuff
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    self.serialPortManager = [ORSSerialPortManager sharedSerialPortManager];
            
            NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
            [nc addObserver:self selector:@selector(serialPortsWereConnected:) name:ORSSerialPortsWereConnectedNotification object:nil];
            [nc addObserver:self selector:@selector(serialPortsWereDisconnected:) name:ORSSerialPortsWereDisconnectedNotification object:nil];
    
    
    // @TODO: Not really using notification yet but its here ready for when I do
    #if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_7)
            [[NSUserNotificationCenter defaultUserNotificationCenter] setDelegate:self];
    #endif
    
    // Get a list of serial ports @TODO: should probably add a refresh button at some point
    [self.serialPorts removeAllItems];
    for (ORSSerialPort* port in self.serialPortManager.availablePorts)
    {
        [self.serialPorts addItemWithObjectValue:port.path];
    }
    
    // Set the log window line limit
    self.logTxt.textContainer.maximumNumberOfLines = logWindowLineLimit;
    // set default update period
    self.updatePeriodMs.stringValue = [@(defaultAutoPollTime) stringValue];
    // auto lock on output enable
    self.lockBtnCell.state = ON;
    // disable all ui buttons that can not be used when not connected
    [self enableButtons:false];
}

/**
  Enable or disable the ui buttons on connect/disconnect
 */
-(void)enableButtons:(bool)yesOrNo
{
    self.autoBtnCell.enabled = yesOrNo;
    self.setCurrentBtn.enabled = yesOrNo;
    self.setVoltageBtn.enabled = yesOrNo;
    self.regDumpBtn.enabled = yesOrNo;
    self.stateBtn.enabled = yesOrNo;
    self.outputBtn.enabled = yesOrNo;
    self.lockBtnCell.enabled = yesOrNo;
    self.setOIPRotectionBtn.enabled = yesOrNo;
    self.setOVProtectionBtn.enabled = yesOrNo;
}

/**
  Add some string to the log window and to the conosle for good measure
 */
- (void)addLog:(NSString*)formatString, ...
{
    va_list args;
    va_start(args, formatString);
    NSString* contents = [[NSString alloc] initWithFormat:formatString arguments:args];
    va_end(args);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSAttributedString* attr = [[NSAttributedString alloc] initWithString:contents];

        [[self.logTxt textStorage] appendAttributedString:attr];
        [self.logTxt scrollRangeToVisible:NSMakeRange([[self.logTxt string] length], 0)];
    });

    NSLog(@"%@", contents);
}

/**
  Format data form the power supply and send it to the log window
 */
- (void)dumpRx:(NSString*)message buffer:(uint16_t[])buffer len:(uint16_t)len
{
    [self addLog:@"RX [%04x] %@: ", len, message];
   
   for (int i = 0; i < len; i++)
   {
       [self addLog:@"%04lx ", (unsigned long)buffer[i]];
   }
   [self addLog:@"\n"];
}

/**
  Do stuff needed when the output state changes also called when the lock button is pressed to sort out the locking of V&I control buttons
 */
- (void)updateOutputState
{
    bool outState = [self getOutputState];
    if (outState != self.outputOn)
    {
        [self addLog:@"Output does not match what was set, OV or OI kicked in?\n"];
    }
    self.outputOn = outState;
    if (self.outputOn == false)
    {
        self.outputBtn.title = @"Output On";
        self.outputStateTxt.stringValue = @"Off";
        self.setVoltageBtn.enabled = true;
        self.setCurrentBtn.enabled = true;
        self.setOVProtectionBtn.enabled = true;
        self.setOIPRotectionBtn.enabled = true;
        self.oVProtectionTxt.enabled = true;
        self.oIProtectionTxt.enabled = true;
    }
    else
    {
        self.outputBtn.title = @"Output Off";
        self.outputStateTxt.stringValue = @"On";
        if (self.lockBtnCell.state == ON)
        {
            self.setVoltageBtn.enabled = false;
            self.setCurrentBtn.enabled = false;
        }
        else
        {
            self.setVoltageBtn.enabled = true;
            self.setCurrentBtn.enabled = true;
        }
        self.setOVProtectionBtn.enabled = false;
        self.setOIPRotectionBtn.enabled = false;
        self.oVProtectionTxt.enabled = false;
        self.oIProtectionTxt.enabled = false;
    }
}

/**
  Get the V&I set points, output state and actuals
 */
- (void)getStatus
{
    if (self.modbusDesc != nil)
    {
        self.outputOn = [self getOutputState];
        [self updateOutputState];
        double voltage = [self getVoltage]/100.0;
        [self addLog:@"Voltage: %f\n", voltage];
        self.voltageTxt.stringValue = [@(voltage) stringValue];
        double current = [self getCurrentLimit]/1000.0;
        [self addLog:@"Current Limit: %f\n", current];
        self.currentTxt.stringValue = [@(current) stringValue];
        double oV = [self getOVProtection]/100.0;
        [self addLog:@"OV: %f\n", oV];
        self.oVProtectionTxt.stringValue = [@(oV) stringValue];
        double oI = [self getOIProtection]/1000.0;
        [self addLog:@"OI: %f\n", oI];
        self.oIProtectionTxt.stringValue = [@(oI) stringValue];
        [self getAndUpdateActuals];
    }
}

/**
  Get the actual V&I, no logging is done here as it can be auto polled which would spam the log window and is not very useful anyway
 */
- (void)getAndUpdateActuals
{
    double voltage = 0.0;
    double current = 0.0;
    double power = 0.0;
    [self getActuals:&voltage amps:&current power:&power];
    [self updateActuals:voltage amps:current power:power];
    bool output = [self getOutputState];
    if (output != self.outputOn)
    {
        [self updateOutputState];
    }
}

/**
  Update the actual strings on the UI
 */
- (void)updateActuals:(double)voltage amps:(double)amps power:(double)power
{
    self.actualVoltageTxt.stringValue = [NSString stringWithFormat:@"%02.02fV", voltage];
    self.actualCurrentTxt.stringValue = [NSString stringWithFormat:@"%01.03fA", amps];
    self.actualPowerTxt.stringValue = [NSString stringWithFormat:@"%01.03fW", power];
}

/**
  Enable or disable auto polling
 */
-(void)enableAutoPoll
{
    int periodValue = [self.updatePeriodMs intValue];
    if (periodValue < minimumAutoPollTime)
    {
        periodValue = minimumAutoPollTime;
    }
    else if (periodValue > maximumAutoPollTime)
    {
        periodValue = maximumAutoPollTime;
    }
    if (periodValue != 0)
    {
        double period = periodValue/1000.0;
        if (self.pollTimer != nil)
        {
            [self.pollTimer invalidate];
            self.pollTimer = nil;
        }
        self.stateBtn.enabled = false;
        self.regDumpBtn.enabled = false;
        //kick off a timer that will get anbd update the actuals, super hacky!
        self.pollTimer = [NSTimer scheduledTimerWithTimeInterval:period target:self selector:@selector(getAndUpdateActuals) userInfo: nil repeats:YES ];
        self.updatePeriodMs.enabled = false;
        self.updatePeriodMs.stringValue = [@(periodValue) stringValue];
    }
    else
    {
        [self addLog:@"Incorrect period specified\n"];
        self.autoBtnCell.state = false;
    }
    
}

/**
  Disable auto polling
 */
-(void)disableAutoPoll
{
    if (self.pollTimer != nil)
    {
        [self.pollTimer invalidate];
        self.pollTimer = nil;
    }
    self.autoBtnCell.state = OFF;
    self.stateBtn.enabled = true;
    self.regDumpBtn.enabled = true;
    self.updatePeriodMs.enabled = true;
}

/**
  Close the connection to the suppy and disable stuff that needs disableing
 */
-(void)closeConnection
{
    if (self.modbusDesc != nil)
    {
        [self disableAutoPoll];
        modbus_close(self.modbusDesc);
        modbus_free(self.modbusDesc);
        self.modbusDesc = nil;
        self.connectBtn.title = @"Connect";
        self.serialPorts.enabled = true;
        [self enableButtons:false];
    }
}

#pragma mark - Get register values
/**
  Get the output state from the supply
 */
-(bool)getOutputState
{
    bool retVal = false;
    if (self.modbusDesc != nil)
    {
        uint16_t buffer[5];
        int rxSize = 0;
        
        rxSize = modbus_read_registers(self.modbusDesc, 0x0001, 1, (uint16_t*)buffer);
        if (rxSize > 0)
        {
            //[self dumpRx:@"Output State" buffer:buffer len:rxSize];
            if (buffer[0] != 0)
            {
                retVal = true;
            }
            else
            {
                retVal = false;
            }
        }
        else
        {
            [self addLog:@"Error reading from modbus device: %s\n", modbus_strerror(errno)];
            [self closeConnection];
        }
    }
    return retVal;
}

/**
  Get the V setpoint from the supply
 */
-(uint32_t)getVoltage
{
    uint32_t retVal = 0;
    if (self.modbusDesc != nil)
    {
        uint16_t buffer[5];
        int rxSize = 0;
        
        rxSize = modbus_read_registers(self.modbusDesc, 0x30, 1, (uint16_t*)buffer);
        if (rxSize > 0)
        {
            [self dumpRx:@"Voltage" buffer:buffer len:rxSize];
            retVal |= buffer[0];
        }
        else
        {
            [self addLog:@"Error reading from modbus device: %s\n", modbus_strerror(errno)];
            [self closeConnection];
        }
    }
    return retVal;
}

/**
  Get the OV setpoint from the supply
 */
-(uint32_t)getOVProtection
{
    uint32_t retVal = 0;
    if (self.modbusDesc != nil)
    {
        uint16_t buffer[5];
        int rxSize = 0;
        
        rxSize = modbus_read_registers(self.modbusDesc, 0x20, 1, (uint16_t*)buffer);
        if (rxSize > 0)
        {
            [self dumpRx:@"OVoltage" buffer:buffer len:rxSize];
            retVal |= buffer[0];
        }
        else
        {
            [self addLog:@"Error reading from modbus device: %s\n", modbus_strerror(errno)];
            [self closeConnection];
        }
    }
    return retVal;
}

/**
  Get the I set point from the supply
 */
-(uint32_t)getCurrentLimit
{
    uint32_t retVal = 0;
    if (self.modbusDesc != nil)
    {
        uint16_t buffer[1];
        int rxSize = 0;
        
        rxSize = modbus_read_registers(self.modbusDesc, 0x31, 1, (uint16_t*)buffer);
        if (rxSize > 0)
        {
            [self dumpRx:@"Current Limit" buffer:buffer len:rxSize];
            retVal |= buffer[0];
        }
        else
        {
            [self addLog:@"Error reading from modbus device: %s\n", modbus_strerror(errno)];
            [self closeConnection];
        }
    }
    return retVal;
}

/**
  Get the I set point from the supply
 */
-(uint32_t)getOIProtection
{
    uint32_t retVal = 0;
    if (self.modbusDesc != nil)
    {
        uint16_t buffer[1];
        int rxSize = 0;
        
        rxSize = modbus_read_registers(self.modbusDesc, 0x21, 1, (uint16_t*)buffer);
        if (rxSize > 0)
        {
            [self dumpRx:@"OV Protection" buffer:buffer len:rxSize];
            retVal |= buffer[0];
        }
        else
        {
            [self addLog:@"Error reading from modbus device: %s\n", modbus_strerror(errno)];
            [self closeConnection];
        }
    }
    return retVal;
}

/**
  Get actual output voltage from the supply
 */
-(uint32_t)getActualVoltage
{
    uint32_t retVal = 0;
    if (self.modbusDesc != nil)
    {
        uint16_t buffer[1];
        int rxSize = 0;
        
        rxSize = modbus_read_registers(self.modbusDesc, 0x10, 1, (uint16_t*)buffer);
        if (rxSize > 0)
        {
            [self dumpRx:@"Actual Voltage" buffer:buffer len:rxSize];
            retVal |= buffer[0];
        }
        else
        {
            [self addLog:@"Error reading from modbus device: %s\n", modbus_strerror(errno)];
            [self closeConnection];
        }
    }
    return retVal;
}

/**
  Get the actual I from the supply
 */
-(uint32_t)getActualCurrent
{
    uint32_t retVal = 0;
    if (self.modbusDesc != nil)
    {
        uint16_t buffer[1];
        int rxSize = 0;
        
        rxSize = modbus_read_registers(self.modbusDesc, 0x11, 1, (uint16_t*)buffer);
        if (rxSize > 0)
        {
            [self dumpRx:@"Actual Current" buffer:buffer len:rxSize];
            retVal |= buffer[0];
        }
        else
        {
            [self addLog:@"Error reading from modbus device: %s\n", modbus_strerror(errno)];
            [self closeConnection];
        }
    }
    return retVal;
}

/**
  Get the actual W from the supply
 */
-(uint32_t)getActualPower
{
    uint32_t retVal = 0;
    if (self.modbusDesc != nil)
    {
        uint16_t buffer[2];
        int rxSize = 0;
        
        rxSize = modbus_read_registers(self.modbusDesc, 0x12, 2, (uint16_t*)buffer);
        if (rxSize > 0)
        {
            [self dumpRx:@"Actual Current" buffer:buffer len:rxSize];
            retVal |= buffer[0];
            retVal |= buffer[1] << 16;
        }
        else
        {
            [self addLog:@"Error reading from modbus device: %s\n", modbus_strerror(errno)];
            [self closeConnection];
        }
    }
    return retVal;
}

/**
  Get all actuals
 */
-(void)getActuals:(double*)voltage amps:(double*)amps power:(double*)power
{
    if (self.modbusDesc != nil)
    {
        uint16_t buffer[4];
        int rxSize = 0;
        
        rxSize = modbus_read_registers(self.modbusDesc, 0x10, 4, (uint16_t*)buffer);
        if (rxSize > 0)
        {
            *voltage = (double)buffer[0] / 100.0;
            *amps = (double)buffer[1]/1000.0;
            uint32_t temp = buffer[2] << 16;
            temp |= buffer[3];
            *power = (double)temp/1000.0;
        }
        else
        {
            [self addLog:@"Error reading from modbus device: %s\n", modbus_strerror(errno)];
            [self closeConnection];
        }
    }
    else
    {
        *voltage = 0.0;
        *amps = 0.0;
        *power = 0.0;
    }
    
}

/**
  Set the voltage set point
 */
-(void) setVoltage:(double)value
{
    uint16_t theValue = value * 100.0;
    if (self.modbusDesc != nil)
    {
        int txSize = 0;
        
        txSize = modbus_write_register(self.modbusDesc, 0x30, theValue);
        if (txSize > 0)
        {
            [self addLog:@"Set Voltage: 0x%04x\n", theValue];
            [self getVoltage];
        }
        else
        {
            [self addLog:@"Error writing to modbus device: %s\n", modbus_strerror(errno)];
            [self closeConnection];
        }
    }
}

/**
  Set the OV set point
 */
-(void) setOVProtection:(double)value
{
    uint16_t theValue = value * 100.0;
    if (self.modbusDesc != nil)
    {
        int txSize = 0;
        
        txSize = modbus_write_register(self.modbusDesc, 0x20, theValue);
        if (txSize > 0)
        {
            [self addLog:@"Set OV: 0x%04x\n", theValue];
            [self getVoltage];
        }
        else
        {
            [self addLog:@"Error writing to modbus device: %s\n", modbus_strerror(errno)];
            [self closeConnection];
        }
    }
}

/**
  Set the I setpoint
 */
-(void) setCurrent:(double)value
{
    uint16_t theValue = value * 1000.0;
    if (self.modbusDesc != nil)
    {
        int txSize = 0;
        
        txSize = modbus_write_register(self.modbusDesc, 0x31, theValue);
        if (txSize > 0)
        {
            [self addLog:@"Set Current: 0x%04x\n", theValue];
            [self getCurrentLimit];
        }
        else
        {
            [self addLog:@"Error writing to modbus device: %s\n", modbus_strerror(errno)];
            [self closeConnection];
        }
    }
}

/**
  Set the OI setpoint
 */
-(void) setOIProtection:(double)value
{
    uint16_t theValue = value * 1000.0;
    if (self.modbusDesc != nil)
    {
        int txSize = 0;
        
        txSize = modbus_write_register(self.modbusDesc, 0x21, theValue);
        if (txSize > 0)
        {
            [self addLog:@"Set OI: 0x%04x\n", theValue];
            [self getCurrentLimit];
        }
        else
        {
            [self addLog:@"Error writing to modbus device: %s\n", modbus_strerror(errno)];
            [self closeConnection];
        }
    }
}

#pragma mark - UI handlers
/**
  Handle connect/disconnect request
 */
- (IBAction)connectButtonPressed:(id)sender
{
    if (self.modbusDesc == nil)
    {
        //should not be running but stop it anyway
        [self disableAutoPoll];
        const char* path = [self.serialPorts.selectedCell.stringValue cStringUsingEncoding:NSUTF8StringEncoding];
        if (path != nil)
        {
            //try and talk to the supply
            self.modbusDesc = modbus_new_rtu(path, 9600, 'N', 8, 1);
            if (self.modbusDesc != nil)
            {
                modbus_set_slave(self.modbusDesc, 1);
                if (modbus_connect(self.modbusDesc) == 0)
                {
                    //woohoo looks like we have connected to something modbusish @TODO: Probably should read id registers to make sure it is the supply we want
                    [self addLog:@"Connected\n"];
                    self.connectBtn.title = @"Disconnect";
                    self.serialPorts.enabled = false;
                    [self enableButtons:true];
                    [self getStatus];
                }
                else
                {
                    [self addLog:@"Error connecting to modbus device: %s\n", modbus_strerror(errno)];
                    modbus_free(self.modbusDesc);
                    self.modbusDesc = nil;
                }
            }
            else
            {
                NSLog(@"Error creating modbus context: %s\n", modbus_strerror(errno));
            }
        }
    }
    else
    {
        [self closeConnection];
    }
}

/**
  Voltage set point button press handler
 */
- (IBAction)voltageButtonPressed:(id)sender
{
    NSString* theString = self.voltageTxt.stringValue;
    
    double voltage = [theString doubleValue];
    if (voltage < minimumVoltage)
    {
        voltage = minimumVoltage;
    }
    else if (voltage > maximumVoltage)
    {
        voltage = maximumVoltage;
    }
    if (voltage >= 0.0)
    {
        [self addLog:@"Setting voltage to %f\n", voltage];
        [self setVoltage:voltage];
        self.voltageTxt.stringValue = [@(voltage) stringValue];
    }
    else
    {
        [self addLog:@"Voltage not a valid value %d\n", self.voltageTxt.stringValue];
    }
    
}

/**
  Over Voltage protection set point button press handler
 */
- (IBAction)oVProtectionButtonPressed:(id)sender
{
    NSString* theString = self.oVProtectionTxt.stringValue;
    
    double voltage = [theString doubleValue];
    if (voltage < minimumVoltage)
    {
        voltage = minimumVoltage;
    }
    else if (voltage > maximumVoltage)
    {
        voltage = maximumVoltage;
    }
    if (voltage >= 0.0)
    {
        [self addLog:@"Setting OV to %f\n", voltage];
        [self setOVProtection:voltage];
        self.oVProtectionTxt.stringValue = [@(voltage) stringValue];
    }
    else
    {
        [self addLog:@"OV not a valid value %d\n", self.voltageTxt.stringValue];
    }
}

/**
  Current set point button press handler
 */
- (IBAction)currentButtonPressed:(id)sender
{
    NSString* theString = self.currentTxt.stringValue;
      
    double current = [theString doubleValue];
    
    if (current < minimumCurrent)
    {
        current = minimumCurrent;
    }
    else if (current > maximumCurrent)
    {
        current = maximumCurrent;
    }
    
      if (current >= 0.0)
      {
          [self addLog:@"Setting current to %f\n", current];
          [self setCurrent:current];
          self.currentTxt.stringValue = [@(current) stringValue];
      }
      else
      {
          [self addLog:@"Current not a valid value %d\n", self.currentTxt.stringValue];
      }
}

/**
  Over current set point button press handler
 */
- (IBAction)oIProtectionButtonPressed:(id)sender
{
    NSString* theString = self.oIProtectionTxt.stringValue;
      
    double current = [theString doubleValue];
    
    if (current < minimumCurrent)
    {
        current = minimumCurrent;
    }
    else if (current > maximumCurrent)
    {
        current = maximumCurrent;
    }
    
      if (current >= 0.0)
      {
          [self addLog:@"Setting OI Protection to %f\n", current];
          [self setOIProtection:current];
          self.oIProtectionTxt.stringValue = [@(current) stringValue];
      }
      else
      {
          [self addLog:@"Current not a valid value %d\n", self.currentTxt.stringValue];
      }
}

/**
  Lock button cell toggle
 */
- (IBAction)lockPressed:(id)sender
{
    [self updateOutputState];
}

/**
  Output button handler, toggles output on or off
 */
- (IBAction)outputButtonPressed:(id)sender
{
    if (self.modbusDesc != nil)
    {
        int payload;
        if (self.outputOn == false)
        {
            payload = 1;
        }
        else
        {
            payload = 0;
        }
        
        int txSize = 0;
        
        txSize = modbus_write_register(self.modbusDesc, 0x0001, payload);
        if (txSize > 0)
        {
            self.outputOn = [self getOutputState];
            [self updateOutputState];
        }
        else
       {
           [self addLog:@"Error writing to modbus device: %s\n", modbus_strerror(errno)];
           [self closeConnection];
       }
        
    }
}

/**
  State button pressed, gets and updates V&I, Output state and actuals
 */
- (IBAction)stateButtonPressed:(id)sender
{
    [self getStatus];
}

/**
  Auto cell toggle handler
 */
- (IBAction)autoPressed:(id)sender
{
    if (self.autoBtnCell.state == ON)
    {
        [self enableAutoPoll];
    }
    else
    {
        [self disableAutoPoll];
    }
}

/**
  Reg dump button pressed, used for reverse engineering some of the registers
 @TODO: should show some sort of progress when reading lots of regsiters
 */
- (IBAction)regDumpButtonPressed:(id)sender
{
    if (self.modbusDesc != nil)
    {
        int chunkSize = regDumpChunkSize;
        uint16_t bigBuffer[regDumpSize];
        int totalSize = 0;
        int lengthOfBuffer = sizeof(bigBuffer)/sizeof(bigBuffer[0]);
        while (totalSize < lengthOfBuffer)
        {
            int rxSize = modbus_read_registers(self.modbusDesc, totalSize, chunkSize, (uint16_t*)(bigBuffer + totalSize));
            if (rxSize > 0)
            {
                totalSize += rxSize;
                if ((lengthOfBuffer - totalSize) < chunkSize)
                {
                    chunkSize = lengthOfBuffer - totalSize;
                }
            }
            else
            {
                [self addLog:@"Error reading from modbus device: %s\n", modbus_strerror(errno)];
                [self closeConnection];
                break;
            }
        }
         [self dumpRx:@"Reg Dump" buffer:bigBuffer len:totalSize];
    }
}

#pragma mark - NSUserNotificationCenterDelegate

#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_7)

- (void)userNotificationCenter:(NSUserNotificationCenter *)center didDeliverNotification:(NSUserNotification *)notification
{
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 3.0 * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [center removeDeliveredNotification:notification];
    });
}

- (BOOL)userNotificationCenter:(NSUserNotificationCenter *)center shouldPresentNotification:(NSUserNotification *)notification
{
    return YES;
}


#endif

#pragma mark - Notifications

- (void)serialPortsWereConnected:(NSNotification *)notification
{
    NSArray *connectedPorts = [notification userInfo][ORSConnectedSerialPortsKey];
    NSLog(@"Ports were connected: %@", connectedPorts);
    [self postUserNotificationForConnectedPorts:connectedPorts];
}

- (void)serialPortsWereDisconnected:(NSNotification *)notification
{
    NSArray *disconnectedPorts = [notification userInfo][ORSDisconnectedSerialPortsKey];
    NSLog(@"Ports were disconnected: %@", disconnectedPorts);
    [self postUserNotificationForDisconnectedPorts:disconnectedPorts];
    
}

- (void)postUserNotificationForConnectedPorts:(NSArray *)connectedPorts
{
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_7)
    if (!NSClassFromString(@"NSUserNotificationCenter")) return;
    
    NSUserNotificationCenter *unc = [NSUserNotificationCenter defaultUserNotificationCenter];
    for (ORSSerialPort *port in connectedPorts)
    {
        NSUserNotification *userNote = [[NSUserNotification alloc] init];
        userNote.title = NSLocalizedString(@"Serial Port Connected", @"Serial Port Connected");
        NSString *informativeTextFormat = NSLocalizedString(@"Serial Port %@ was connected to your Mac.", @"Serial port connected user notification informative text");
        userNote.informativeText = [NSString stringWithFormat:informativeTextFormat, port.name];
        userNote.soundName = nil;
        [unc deliverNotification:userNote];
    }
#endif
}

- (void)postUserNotificationForDisconnectedPorts:(NSArray *)disconnectedPorts
{
#if (MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_7)
    if (!NSClassFromString(@"NSUserNotificationCenter")) return;
    
    NSUserNotificationCenter *unc = [NSUserNotificationCenter defaultUserNotificationCenter];
    for (ORSSerialPort *port in disconnectedPorts)
    {
        NSUserNotification *userNote = [[NSUserNotification alloc] init];
        userNote.title = NSLocalizedString(@"Serial Port Disconnected", @"Serial Port Disconnected");
        NSString *informativeTextFormat = NSLocalizedString(@"Serial Port %@ was disconnected from your Mac.", @"Serial port disconnected user notification informative text");
        userNote.informativeText = [NSString stringWithFormat:informativeTextFormat, port.name];
        userNote.soundName = nil;
        [unc deliverNotification:userNote];
    }
#endif
}

- (void)handleRxData:(NSData *)data
{
    uint8_t *bytes = (uint8_t*)data.bytes;
    NSMutableString *bytesStr= [NSMutableString stringWithCapacity:sizeof(bytes)*2];
    for(int i=0;i<sizeof(bytes);i++){
        NSString *resultString =[NSString stringWithFormat:@"%02lx",(unsigned long)bytes[i]];
        [bytesStr appendString:resultString];
    }
    NSLog(@"%@", bytesStr);
    [self addLog: bytesStr];
    [self addLog: @"\n"];
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}


@end
