//
//  ViewController.h
//  PowerSupply
//
//  Created by Christopher Cope on 19/06/2020.
//  Copyright © 2020 Christopher Cope. All rights reserved.
//

/**
 Thanks to http://nightflyerfireworks.com/home/fun-with-cheap-programable-power-supplies
  for doing the majority of the hard work reverse engineering the registers

     power switch address     = 0x0001(R/W)
     Protect state address     = 0x0002(R)
     Model address            = 0x0003(R)
     Class detail            = 0x0004(R)
     Decimals                = 0x0005(R)
     Voltage                = 0x0010(R)
     Current                = 0x0011(R)
     Power(wattage)        = 0x0012(R) 4 byte response
     Power cal                = 0x0014(R/W?) 4 byte response
     Protect Voltage        = 0x0020(R)
     Protect Current        = 0x0021(R)
     Protect Power            = 0x0022(R) 4 byte response
     Set Voltage            = 0x0030
     Set Current            = 0x0031
     Set Time span            = 0x0032
     Power state            = 0x8801
     Default show?            = 0x8802
     SCP?                = 0x8803
     Buzzer                = 0x8804
     Device?                = 0x9999
     SD Time?                = 0xCCCC

     These are listed but not used by the source code. more testing needs to be done to find out there function.
     UL                    = 0xC110(R) 4 byte
     UH                    = 0xC11E(R) 4 byte
     IL                    = 0xC120(R) 4 byte
     IH                    = 0xc12E(R) 4 byte
 */

#import <Cocoa/Cocoa.h>
#import <ORSSerial/ORSSerial.h>
#import <modbus.h>

@interface ViewController : NSViewController <NSUserNotificationCenterDelegate>

@property (nonatomic, strong) IBOutlet NSButton* connectBtn;
@property (nonatomic, strong) IBOutlet NSButton* setVoltageBtn;
@property (nonatomic, strong) IBOutlet NSButton* setCurrentBtn;
@property (nonatomic, strong) IBOutlet NSButton* setOVProtectionBtn;
@property (nonatomic, strong) IBOutlet NSButton* setOIPRotectionBtn;
@property (nonatomic, strong) IBOutlet NSButton* outputBtn;
@property (nonatomic, strong) IBOutlet NSButton* stateBtn;
@property (nonatomic, strong) IBOutlet NSButtonCell* autoBtnCell;
@property (nonatomic, strong) IBOutlet NSButton* regDumpBtn;
@property (nonatomic, strong) IBOutlet NSComboBox* serialPorts;
@property (nonatomic, strong) IBOutlet NSTextField* voltageTxt;
@property (nonatomic, strong) IBOutlet NSTextField* currentTxt;
@property (nonatomic, strong) IBOutlet NSTextField* oVProtectionTxt;
@property (nonatomic, strong) IBOutlet NSTextField* oIProtectionTxt;
@property (nonatomic, strong) IBOutlet NSTextView* logTxt;
@property (nonatomic, strong) IBOutlet NSTextField* outputStateTxt;
@property (nonatomic, strong) IBOutlet NSButtonCell* lockBtnCell;

@property (nonatomic, strong) IBOutlet NSTextField* actualVoltageTxt;
@property (nonatomic, strong) IBOutlet NSTextField* actualCurrentTxt;
@property (nonatomic, strong) IBOutlet NSTextField* actualPowerTxt;
@property (nonatomic, strong) IBOutlet NSTextField* updatePeriodMs;


@property (nonatomic, strong) NSTimer* pollTimer;

@property (nonatomic) bool outputOn;


@property (nonatomic, strong) ORSSerialPortManager *serialPortManager;
@property (nonatomic) modbus_t* modbusDesc;

- (void)dumpRx:(NSString*)message buffer:(uint16_t[])buffer len:(uint16_t)len;
- (void)addLog:(NSString*)theString, ...;
- (void)getStatus;
- (void)handleRxData:(NSData *)data;

- (IBAction)connectButtonPressed:(id)sender;
- (IBAction)voltageButtonPressed:(id)sender;
- (IBAction)currentButtonPressed:(id)sender;
- (IBAction)oVProtectionButtonPressed:(id)sender;
- (IBAction)oIProtectionButtonPressed:(id)sender;
- (IBAction)outputButtonPressed:(id)sender;
- (IBAction)stateButtonPressed:(id)sender;
- (IBAction)autoPressed:(id)sender;
- (IBAction)regDumpButtonPressed:(id)sender;
- (IBAction)lockPressed:(id)sender;


@end

