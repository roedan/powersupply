//
//  MainWindow.h
//  PowerSupply
//
//  Created by Christopher Cope on 22/06/2020.
//  Copyright © 2020 Christopher Cope. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface MainWindow : NSWindowController <NSWindowDelegate>

@end

NS_ASSUME_NONNULL_END
