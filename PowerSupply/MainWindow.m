//
//  MainWindow.m
//  PowerSupply
//
//  Created by Christopher Cope on 22/06/2020.
//  Copyright © 2020 Christopher Cope. All rights reserved.
//

#import "MainWindow.h"

@interface MainWindow ()

@end

@implementation MainWindow

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

- (BOOL)windowShouldClose:(NSWindow *)sender
{
    [[NSApplication sharedApplication] terminate:nil];
    return true;
}

@end
