//
//  AppDelegate.m
//  PowerSupply
//
//  Created by Christopher Cope on 19/06/2020.
//  Copyright © 2020 Christopher Cope. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
