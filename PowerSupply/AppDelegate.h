//
//  AppDelegate.h
//  PowerSupply
//
//  Created by Christopher Cope on 19/06/2020.
//  Copyright © 2020 Christopher Cope. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

