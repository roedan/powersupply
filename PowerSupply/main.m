//
//  main.m
//  PowerSupply
//
//  Created by Christopher Cope on 19/06/2020.
//  Copyright © 2020 Christopher Cope. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
    }
    return NSApplicationMain(argc, argv);
}
