# PowerSupply

Dirty hacky app to control the Hanmatek HM305P power supply in OSX.
![The Beast](docs/HM305P.jpg)

Needs libmodbus installing using brew or similar

## Build instructions
### Install libmodbus
The source for libmodbus can be found here: [https://libmodbus.org/](https://libmodbus.org/)

However, I use brew ([https://brew.sh](https://brew.sh)) so something like `brew install libmodbus` should get it installed for you
### Get and build the code
Not much to say apart from:
1. Clone this repo
2. Open the project in xcode
3. Hit build

## How to use
### The basics
![Basic GUI Window](docs/MainWindow.png)
1. Choose your serial port from the "Serial Port" drop down box
2. Click "Connect"
3. Assumming everything went ok you can now set the "Voltage" set point and "Current" set point
4. Click "Output On" to toggle the supply output

### Locking
You can lock or unlock setting of V&I whilst the output is on (stops you accidently pumping 30V into your 1.8V system ;) )

Simply toggle the "Lock" tick box

### Auto polling
You can get the app to poll the actual V, I & W values and updatethe "Actuals" data
1. Set the required update rate in the "Auto Update Actuals (ms)" text box
2. Click the "On/Off" tick box
Note: The update value limits are from 100ms to 30s, these are defined in [ViewController.m](PowerSupply/ViewController.m)

## Things to do
* Fix the various UI warning for locale!
* See @TODO comments in the code

## Thanks
* Thanks to [Night Flyer Networks](http://nightflyerfireworks.com/home/fun-with-cheap-programable-power-supplies) for started the reverse engineering of the power supply registers
* [libmodbus](https://libmodbus.org/) for implemeting libmodbus so I didn't have to
* Icon made by [Fereepik](https://www.flaticon.com/authors/freepik) from [Flaticon](https://www.flaticon.com/)
